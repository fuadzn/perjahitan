<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once("Secure_area.php");
class ManageTable extends Secure_area
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mesin', '', TRUE);
		$this->load->library('phpass');
	}

	public function index()
	{
		$result['Machine'] = $this->Mesin->get_all_machine()->result();
		// $result['User'] = $this->M_Login->get_all_user()->result();
		$this->load->view('manageTable', $result);
	}

	function edit_machine()
	{
		$id = $this->input->post('id');
		$datajson = $this->Mesin->get_id_machine($id)->result();
		echo json_encode($datajson);
	}

	function update_machine()
	{
		// $old_name = $this->input->post('old_name');
		$where = array('machine_id' => $this->input->post('machine_id'));
		$tableName= $this->input->post('old_name');
		$newTableName= $this->input->post('new_name');
		$data = array(
			'machine_name' => $this->input->post('new_name'),
		);
		if($where == null){
		$msg = '<div class="alert alert-danger alert-dismissible"> <i class="ti-user"></i> Data gagal diubah!
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    </div>';
		}else{
			$this->Mesin->update_table('list_machine', $data, $where);
			$this->Mesin->rename_table($newTableName, $tableName);

		$msg = '<div class="alert alert-success alert-dismissible"> <i class="ti-user"></i> Data Berhasil Diubah!
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    </div>';
		}
		$this->session->set_flashdata('success_msg', $msg);
		redirect('ManageTable');
	}

	function delete_table()
	{
		$id = $this->input->post('id');
		
			$save = $this->Mesin->deleteTable($id);

			if($save >= 1){
				$data = array('status' => 'success');
			}else{
				$data = array('status' => 'failed');
			}
	
			echo json_encode($data);
	}
	// function login_check($username)
	// {
	// 	$this->load->view('manageUser');
	// }
}
