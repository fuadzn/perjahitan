<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once("Secure_area.php");
class Table extends Secure_area
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mesin', '', TRUE);
		$this->load->library('phpass');
	}

	public function index()
	{
		// $table = 'Mesin2';
		// $result = $this->Mesin->get_mesin();
		$this->load->view('table');
	}
	
	public function table_view($table)
	{
		$data['tabel'] = $table;
		$this->load->view('table', $data);
	}

	public function get_table($table)
	{
		$line  = array();
		$line2 = array();
		$row2  = array();
		$i = 1;
		// $tgl_awal1 = preg_replace("/[^0-9.-]/", "", $this->input->post('date_picker'));
		// $tgl_akhir1 = preg_replace("/[^0-9.-]/", "", $this->input->post('date_picker_akhir'));
		$tgl_awal = date('Y-m-d', strtotime($this->input->post('tgl')));
		$tgl_akhir = date('Y-m-d', strtotime($this->input->post('tgl_akhir')));
		// $table = $this->input->post('get_tabel');
		// print_r($table);
		// die();
		$dataTable =  $this->Mesin->get_mesin($tgl_awal,$tgl_akhir,$table)->result();
		foreach ($dataTable as $value) {
			$row2['no'] = $i++;
			$row2['temp'] = $value->temp;
			$row2['hum'] = $value->hum;

			$line2[] = $row2;
		}
		$line['data'] = $line2;

		echo json_encode($line);
	}
}
