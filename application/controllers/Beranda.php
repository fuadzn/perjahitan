<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once ("Secure_area.php");
class Beranda extends Secure_area {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mesin', '', TRUE);
		$this->load->library('phpass');
	}
	
	public function index()
	{	
		$result['Mesin'] = $this->Mesin->get_all_mesin()->result();
		// $result = $this->Mesin->get_mesin();
		$this->load->view('beranda', $result);
	}

	public function get_error($table){
		// print_r($table);
		// die();
		$dataError = $this->Mesin->get_error_machine($table)->result();
		$result = false;
		foreach ($dataError as $res){
			if ($res->hum == 0) {
				$result = 'box-table';
			} else{
				$result = 'error-machine';
			}
		}

		echo json_encode(array(
			'succes' => true,
			// 'color' => $dataColor
			'error' => $result,
			'data' => $dataError
		));

	}
}
