<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class LupaPassword extends CI_Controller
{
  public function __construct()
	{
		parent::__construct();
		if($this->uri->total_segments() === 0){
	        redirect('Login','refresh');
	  }
		require APPPATH.'libraries/PHPMailer/src/Exception.php';
		require APPPATH.'libraries/PHPMailer/src/PHPMailer.php';
		require APPPATH.'libraries/PHPMailer/src/SMTP.php';
		$this->load->model('M_Login', '', TRUE);
	}

  function check_data()
  {
    $data = array(
      'username_fp' => $this->input->post('username_fp'),
      'email_fp' => $this->input->post('email_fp')
    );

    $check_username = $this->M_Login->check_username($data['username_fp'])->num_rows();
    $check = $this->M_Login->check_data($data['username_fp'], $data['email_fp'])->num_rows();
    if ($check_username == 0) {
      $result = array('success' => false, 'msg' => 'Username Tidak Terdaftar');
    } else if ($check == 0) {
      $result = array('success' => false, 'msg' => 'Username dan Email Tidak Cocok!');
    } else {
      $result = array('success' => true);
    }


    echo json_encode($result);
  }

  public function email_reset_password_validation()
  {
    $this->load->helper('string');
    $this->form_validation->set_rules('username_fp', 'USERNAME_FP', 'required');
    $this->form_validation->set_rules('email_fp', 'EMAIL_fp', 'required');
    if ($this->form_validation->run()) {
      $data = array(
        'username_fp' => $this->input->post('username_fp'),
        'email_fp' => $this->input->post('email_fp')
      );
      $datajson = $this->M_Login->check_data($data['username_fp'], $data['email_fp'])->result();

      foreach ($datajson as $row) {
        $email = $row->email;
        $reset_key =  random_string('alnum', 50);
      }

      $data = $this->M_Login->check_data($data['username_fp'], $data['email_fp'])->row();
      $result['email'] = $data->email;
      $this->M_Login->update_reset_key($email, $reset_key);

      // PHPMailer
      $mail = new PHPMailer();
      $mail->isSMTP();
      $mail->Host     = 'smtp.gmail.com';
      $mail->SMTPAuth = true;
      $mail->Username = 'fajaralisina13@gmail.com';
      $mail->Password = 'Boeing737';
      // $mail->SMTPSecure = 'ssl';
      $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
      $mail->Port     = 465;
      $mail->SMTPOptions = array(
        'ssl' => array(
          'verify_peer' => false,
          'verify_peer_name' => false,
          'allow_self_signed' => true
        )
      );
      $mail->isHTML(true);

      $mail->setFrom("fajaralisina13@gmail.com", "Permintaan Ganti Password");
      // $mail->addAddress($email);
      $mail->addAddress($email);
      // Email subject
      $mail->Subject = "Permintaan Ganti Password";
      $message = "<p>Anda melakukan permintaan reset password</p>";
      $message .= "<a href='" . site_url('LupaPassword/reset_password/' . $reset_key) . "'>klik reset password</a>";
      $mail->Body = $message;

      // Send email
      if (!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
      } else {
        echo 'Message has been sent';
      }
    } else {
      $this->load->view('login');
    }
  }

  public function reset_password()
  {
    $reset_key = $this->uri->segment(3);

    if (!$reset_key) {
      die('Jangan Dihapus');
    }

    if ($this->M_Login->check_reset_key($reset_key) == 1) {
      $data['username'] = $this->M_Login->get_username($reset_key)->result();
      $this->load->view('Reset_Password', $data);
    } else {
      die("reset key salah");
    }
  }

  public function reset_password_validation()
  {
    $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|matches[retype_password]');
    $this->form_validation->set_rules('retype_password', 'Retype Password', 'required|min_length[6]|matches[password]');

    if ($this->form_validation->run()) {
      $reset_key = $this->input->post('reset_key');
      $password = $this->input->post('password');
      $password2 = password_hash($password, PASSWORD_BCRYPT);
      if ($this->M_Login->reset_password($reset_key, $password2)) {
        $result = array('success' => true, 'msg' => 'Kata sandi berhasil di ubah');
      } else {
        $result = array('success' => false, 'msg' => 'Error!');
      }
    } else {
      $result = array('success' => false);
    }
    echo json_encode($result);
  }
}
