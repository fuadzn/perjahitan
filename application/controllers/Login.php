<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Login', '', TRUE);
		$this->load->library('phpass');
	}

	function index()
	{
		if ($this->M_Login->is_logged_in()) {
			redirect('beranda');
		} else {
			$this->form_validation->set_rules('username', 'Username', 'callback_login_check');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

			if ($this->form_validation->run() == FALSE) {
				$data['username'] = $this->input->post('username');
				$this->load->view('login', $data);
			} else
				redirect('beranda');
		}
	}

	function login_check($username)
	{
		$password = $this->input->post("password");

		$user = $this->M_Login->get_user($username)->result();
		$status = 0;

		$created_date = date("Y-m-d H:i:s");

		foreach ($user as $row) {
			$status = $row->status_user;
			$created_date = substr($row->created_date, 0, 10);
		}
		
		$expired_date = date("Y-m-d", strtotime('1 year', strtotime($created_date)));;
		$created_date = date_create($created_date);
		$expired_date = date_create(($expired_date));

		$today = date_create(date("Y-m-d H:i:s"));

		$diffdate = date_diff( $today, $expired_date);
		$diffdate = strval($diffdate->format("%R%a"));
		if (substr($diffdate, 0, 1) == '-') {
			$username = "";
			foreach ($user as $row) {
				$userid = $row->userid;
			}
			$where = array('userid' => $userid);
		$data = array(
			'status_user' => 0,
		);
			$this->M_Login->update_user('user', $data, $where);
		}

		if (count($user) == 0) {		
			$this->form_validation->set_message('login_check', '<p class="px-2" style="color:red;">Username/Password salah!</p>');
			return false;
		} else {
			foreach ($user as $row) {
				$hashed = $row->password;
				$userid = $row->userid;
			}

			if ($this->phpass->check($password, $hashed)) {
				if ($status == 0) {
					$this->form_validation->set_message('login_check', '<p class="px-2" style="color:red;">Akun anda NonAktif!</p>');
					return false;
				} else {
					$this->session->set_userdata('userid', $userid);
					return true;
				}
			} else {
				// if ($status == 0) {
				// 	$this->form_validation->set_message('login_check', '<p class="px-2" style="color:red;">Akun anda NonAktif!</p>');
				// 	return false;
				// } else {
					$this->form_validation->set_message('login_check', '<p class="px-2" style="color:red;">Username/Password salah!</p>');
					return false;
				// }
			}
		}
	}

	function ajax_check()
	{
		if ($this->M_Login->is_logged_in()) {
			echo json_encode(1);
		} else {
			echo json_encode(0);
		}
	}

	function check_login()
	{
		if ($this->M_Login->logged_id()) {
			//jika memang session sudah terdaftar
			redirect('beranda');
		} else {
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			//jika session belum terdaftar
			if ($this->form_validation->run() == false) {
				$this->load->view('login');
			} else {
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$checking = $this->M_Login->check_login($username, $password);
				if ($checking == true) {
					foreach ($checking as $apps) {
						$session_data = array(
							'userid'   => $apps->userid,
							'username' => $apps->username,
							'password' => $apps->password,
						);
						$this->session->set_userdata($session_data);
						redirect('beranda');
					}
				} else {
					$this->load->view('login');
				}
			}
		}
	}


	public function registrasi()
	{
		$data = array(
			'username' => $this->input->post('usernamer'),
			'email' => $this->input->post('email'),
			'password' => password_hash($this->input->post('passwordr'), PASSWORD_BCRYPT)
		);

		if ($this->input->post('usernamer') && $this->input->post('passwordr') != null) {
			$result = $this->M_Login->registrasi($data);
			$result = array('success' => true);
		} else {
			$result = array('success' => false, 'msg' => 'Harap semua data diisi');
		}

		echo json_encode($result);
	}
}
