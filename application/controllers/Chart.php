<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once("Secure_area.php");
class Chart extends Secure_area
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mesin', '', TRUE);
		$this->load->library('phpass');
	}

	public function index()
	{
		// $result['Mesin'] = $this->Mesin->get_mesin()->result();
		// $result = $this->Mesin->get_mesin();
		$this->load->view('chart');
	}

	public function chart_view($table)
	{
		$data['tabel'] = $table;
		$this->load->view('chart', $data);
	}

	public function get_perbandingan($table)
	{

		$tgl_awal = $this->input->post('tgl');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$datajson = $this->Mesin->get_data($table, $tgl_awal, $tgl_akhir)->result();
		// print_r($datajson);
		// die();
		// $datachart = "";
		// $dataColor = array();
		// $dataset = array();
		// $i = 0;
		$temp = array();
		$hum = array();
		$date = array();
		foreach ($datajson as $index => $row) {
			// $color = '#' . $this->random_color();
			$temp[$index] = $row->temp;
			$hum[$index] = $row->hum;
			$date[$index] = $row->time;
			// $hum = array($row->hum);
			// array_push($dataColor, $color);
		}
		// print_r($temp);
		// die();

		echo json_encode(array(
			'succes' => true,
			'temp' => $temp,
			'hum' => $hum,
			'date' => $date,
			// 'color' => $dataColor
		));
	}

	private function random_color_part() {
    	return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}

	private function random_color() {
	    return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
	}

	public function get_widget($machine){
		$dataError = $this->Mesin->get_error_machine($machine)->result();
		$result = false;
		foreach ($dataError as $res){
				$result = $res->temp;
		}

		echo json_encode(array(
			'succes' => true,
			// 'color' => $dataColor
			'temp' => $result,
			'data' => $dataError
		));

	}
}
