<?php
class Secure_area extends CI_Controller 
{
	/*
	Controllers that are considered secure extend Secure_area, optionally a $module_id can
	be set to also check if a user can access a particular module in the system.
	*/
	function __construct($module_id=null)
	{
		parent::__construct();	
		$this->load->model('M_Login','',TRUE);
		if(!$this->M_Login->is_logged_in())
		{
			redirect('login');
		}

		$logged_in_user_info=$this->M_Login->get_logged_in_user_info();
		// $role_id = $this->M_user->get_role_id()->row()->roleid;
		//$data['allowed_modules']=$this->Module->get_allowed_modules($logged_in_employee_info->person_id);
		$data['user_info']=$logged_in_user_info;
		// $data['role_id']=$logged_in_user_info->role_id;
		$this->load->vars($data);
	}
}
?>