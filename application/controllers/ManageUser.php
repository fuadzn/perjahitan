<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once ("Secure_area.php");
class ManageUser extends Secure_area {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->model('M_Login', '', TRUE);
		$this->load->library('phpass');
	}
	
	public function index()
	{
		$result['User'] = $this->M_Login->get_all_user()->result();
		$this->load->view('manageUser', $result);
    }
    
    public function registrasi()
	{
		$data = array(
			'username' => $this->input->post('usernamer'),
			'password' => password_hash($this->input->post('passwordr'), PASSWORD_BCRYPT)
		);

		if($this->input->post('usernamer') && $this->input->post('passwordr') != null)
		{
			$result = $this->M_Login->registrasi($data);
			$result = array('success' => true);
		} else{
			$result = array('success' => false, 'msg' => 'Harap semua data diisi');
		}

		echo json_encode($result);
	}

	// function login_check($username)
	// {
	// 	$this->load->view('manageUser');
	// }

	function get_user_id()
	{
		$id = $this->input->post('id');
		$datajson = $this->M_Login->get_id_user($id)->result();
		echo json_encode($datajson);
	}

	function delete_user()
	{
		$id = $this->input->post('id');
		
			$save = $this->M_Login->deleteUser($id);

			if($save >= 1){
				$data = array('status' => 'success');
			}else{
				$data = array('status' => 'failed');
			}
	
			echo json_encode($data);
	}

	function aktif_user(){
		$id = $this->input->post('id');
		$data = array(
			'status_user' => '1'
		);

		$date_today = date_create(date("Y-m-d H:i:s"));

		$save = $this->db->query("UPDATE user SET status_user = 1, created_date = now()  WHERE userid='$id' ");

		if($save >= 1){
			$data = array('status' => 'success');
		}else{
			$data = array('status' => 'failed');
		}

		echo json_encode($data);
	}

	function nonaktif_user(){
		$id = $this->input->post('id');
		$data = array(
			'status_user' => '0'
		);
		$save = $this->db->query("UPDATE user SET status_user = 0 WHERE userid='$id' ");
		if($save >= 1){
			$data = array('status' => 'success');
		}else{
			$data = array('status' => 'failed');
		}

		echo json_encode($data);
	}

	function update_user()
	{
		// $old_name = $this->input->post('old_name');
		$where = array('userid' => $this->input->post('userid'));
		// print_r($old_name);
		// die();
		$data = array(
			'username' => $this->input->post('username'),
			'role_id' => $this->input->post('role_id'),
		);
		if($where == null){
		$msg = '<div class="alert alert-danger alert-dismissible"> <i class="ti-user"></i> Data gagal diubah!
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    </div>';
		}else{
			$this->M_Login->update_user('user', $data, $where);

		$msg = '<div class="alert alert-success alert-dismissible"> <i class="ti-user"></i> Data Berhasil Diubah!
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    </div>';
		}
		$this->session->set_flashdata('success_msg', $msg);
		redirect('ManageUser');
	}
}