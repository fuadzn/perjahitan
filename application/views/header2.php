<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Beranda</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
</head>
<style>
        body {
            font-family: "Lato", sans-serif;
        }

        a {
            color: #4a4a4a;
        }

        a:hover{
            color: #4a4a4a;
            text-decoration: unset;
        }

        .main-head{
            height: 150px;
            background: #FFF;
        
        }

        .header{
            width: 100%;
            height: 70px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background: lightslategrey;
        }

        .sidenav {
            height: 100%;
            background-color: #fff;
            /* overflow-x: hidden; */
            box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.25);
            /* padding-top: 20px; */
            /* width: 10%; */
            position: fixed;
            z-index: 1;
            top: 70px;
            left: 0;
        }


        .main {
            padding: 0px 10px;
        }

        @media screen and (max-height: 450px) {
            .sidenav {padding-top: 15px;}
        }

        @media screen and (min-width: 768px){
            /* .main{
                margin-left: 40%; 
            } */

        }


        .login-main-text{
            margin-top: 20%;
            padding: 60px;
            color: #fff;
        }

        .login-main-text h2{
            font-weight: 300;
        }
        .logoutBtn{
            float: right;
            transform: translateY(18px);
            margin-right: 16px;
            border: solid 2px;
            border-radius: 5px;
            height: 35px;
            width: 99px;
            color: #4A4A4A;
        }
        
        .logoutBtn:hover{
            background-color: lightgrey;
        }
        .sidenavBtn{
            border: unset;
            height: 35px;
            width: 100%;
            text-align: left;
            background-color: #fff;
        }
        :target {
            animation: highlight 1s ease;  
            transform: translateX(20px);     
        }
        @keyframes highlight {
            0% { border-left-color: red; }
            100% { border-left-color: white; }
        }
        section{
            margin: 80px auto;
        }
        .table_id_wrapper{
            margin-left: 16em;
        }
    </style>
<body>
    <div class="header">
    <img src="<?php echo base_url('assets/images/Medion.png') ?>" width="auto" height="60px" style="margin: 4px;"></img>
    