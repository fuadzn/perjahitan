<?php $this->load->view('header') ?>
<section class="col-9">
    <h1> Data Tabel Mesin</h1>
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <form class="form">

                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="input-group">
                                    <input type="hidden" id="get_tabel" name="get_tabel" value="<?= $tabel ?>">
                                    <input autocomplete="off" type="text" id="date_picker" class="form-control" placeholder="yyyy-mm-dd" name="tgl" value="<?= date('Y-m-d') ?>">
                                </div>
                            </div>
                            s/d
                            <div class="col-md-2">
                                <div class="input-group">
                                    <input autocomplete="off" type="text" id="date_picker_akhir" class="form-control" placeholder="yyyy-mm-dd" name="tgl_akhir" value="<?= date('Y-m-d') ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary btn-block" type="button" onclick="getData()">Cari</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br>
    <table id="table_id" class="display">
        <thead>
            <tr>
                <th class="col">No</th>
                <th class="col">temp</th>
                <th class="col">hum</th>
            </tr>
        <tbody>
        </tbody>
        </thead>
    </table>
</section>
</div>

<?php
$this->load->view('footer');
?>
</body>

</html>
<!-- <script src="node_modules\jquery\dist/jquery.js"></script>
<script src="node_modules\jquery\dist/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script> -->
<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> -->
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script> -->
<script type='text/javascript'>
    var table_mesin

    // Set global counter variable to verify event instances
    // var nCounter = 0;

    // Set up event handler to produce text for the window focus event
    window.addEventListener("focus", function(event) {
        // getData();
        table_mesin.ajax.reload();
    }, false);
    $(document).ready(function() {
        var myVar = setInterval(table_mesin, 3000);
        var get_tabel = $('#get_tabel').val();
        console.log(get_tabel);

        table_mesin = $('#table_id').DataTable({
            // pagingType: "full_numbers",
            lengthChange: true,
            dom: 'Bfrtip',
            buttons: [{
                extend: 'pdfHtml5',
                text: 'Download PDF',
            }],
            lengthChange: true,
            // paging: false,
            // scrollY: 100,
            // scrollX: 200,
            // searching: true
            ajax: {
                "url": '<?php echo site_url('table/get_table/')?>' + get_tabel,
                "type": 'POST',
                "data": function(d) {
                    return {
                        tgl: $('#date_picker').val(),
                        tgl_akhir: $('#date_picker_akhir').val(),
                    };
                },
                "dataSrc": function(json) {
                    Swal.close();
                    return json.data;
                }
            },
            // displayLength: -1,
            columns: [{
                    data: "no"
                },
                {
                    data: "temp"
                },
                {
                    data: "hum"
                }
                // { data: "email_alter" }
            ],
            // order: [
            //     [0, 'asc']
            // ]
        });
        $('#date_picker').datepicker({
            format: "yyyy-mm-dd",
            endDate: "current",
            autoclose: true,
            orientation: "bottom auto"
        });

        $('#date_picker_akhir').datepicker({
            format: "yyyy-mm-dd",
            endDate: "current",
            autoclose: true,
            orientation: "bottom auto"
        });
    });

    function getData() {
        var date1 = new Date($("#date_picker").val());
        var date2 = new Date($("#date_picker_akhir").val());
        var diff = date2 - date1;
        var txt_date1 = $("#date_picker").val();
        var txt_date2 = $("#date_picker_akhir").val();

        if (diff < 0) {
            $("#date_picker").val(txt_date2);
            $("#date_picker_akhir").val(txt_date1);
        }
        loadingSwal("Mengambil Data.");
        table_mesin.ajax.reload();
    }

    function loadingSwal(get_string) {
        Swal.fire({
            title: get_string,
            html: 'Harap Menunggu.',
            allowOutsideClick: false,
            // timer: 2000,
            // timerProgressBar: true,
            onBeforeOpen: () => {
                Swal.showLoading()
                // timerInterval = setInterval(() => {
                // Swal.getContent().querySelector('b')
                //  .textContent = Swal.getTimerLeft()
                // }, 100)
            },
            onClose: () => {
                // clearInterval(timerInterval)
            }
        }).then((result) => {
            if (result.dismiss === Swal.DismissReason.timer)
                console.log('I was closed by the timer')
        });
    }

    function formatNumber(num) {
        if (num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        } else {
            return "0";
        }
    }
</script>