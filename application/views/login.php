<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Login</title>

    <!-- <link rel="stylesheet" type="text/css" href="node_modules\bootstrap\dist\css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="node_modules\bootstrap\dist\css/bootstrap.min.css">
    <script rel="stylesheet" type="text/css" href="node_modules\jquery\dist/jquery.min.js"> </script>
    <script rel="stylesheet" type="text/css" href="node_modules\popper.js\dist/popper.min.js"></script>
    <script rel="stylesheet" type="text/css" href="node_modules\bootstrap\dist\js/bootstrap.min.js"></script> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->

</head>
<style>
    body {
        font-family: "Lato", sans-serif;
    }



    .main-head {
        height: 150px;
        background: #FFF;

    }

    .sidenav {
        height: 100%;
        background-color: #000;
        overflow-x: hidden;
        padding-top: 20px;
    }


    .main {
        padding: 0px 10px;
    }

    .login-main-text {
        margin-top: 20%;
        padding: 60px;
        color: #fff;
    }

    @media screen and (max-height: 450px) {
        .sidenav {
            padding-top: 15px;
        }
    }

    @media screen and (max-width: 450px) {
        .login-form {
            margin-top: 10%;
        }

        .register-form {
            margin-top: 10%;
        }
    }

    @media screen and (min-width: 768px) {
        .main {
            margin-left: 40%;
        }

        .sidenav {
            width: 40%;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
        }

        .login-form {
            margin-top: 50%;
        }

        .register-form {
            margin-top: 20%;
        }
    }

    @media screen and (max-width: 1000px) {
        .login-main-text {
        margin-top: unset;
        padding: 25px;
        color: #fff;
    }
    }

    .login-main-text h2 {
        font-weight: 300;
    }

    .btn-black {
        background-color: #000 !important;
        color: #fff;
    }
</style>

<body>
    <div class="sidenav">
        <div class="login-main-text">
            <h2>Application<br> Login Page</h2>
            <p>Login or register from here to access.</p>
        </div>
    </div>
    <div class="main">
        <div class="col-md-6 col-sm-12">
            <div class="login-form">
                <!-- <form>
                <div class="form-group">
                    <label>User Name</label>
                    <input type="text" class="form-control" placeholder="User Name">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-black">Login</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Register</button>
            </form> -->
                <section class="row flexbox-container">

                    <div class="card-content">
                        <p class="px-2">Selamat datang user, silahkan login</p>
                        <div class="card-body pt-1">
                            <?php
                            $attributes = array('class' => '', 'id' => 'loginform');
                            echo form_open('login', $attributes);
                            ?>
                            <fieldset class="form-label-group form-group position-relative has-icon-left">
                                <input type="text" class="form-control" id="username" name="username" placeholder="Username" required <?php
                                                                                                                                        if ($username) {
                                                                                                                                            echo 'value="' . $username . '"';
                                                                                                                                        } else {
                                                                                                                                            echo 'autofocus';
                                                                                                                                        }
                                                                                                                                        ?>>
                                <div class="form-control-position">
                                    <i class="feather icon-user"></i>
                                </div>
                                <!-- <label for="username">Username</label> -->
                            </fieldset>

                            <fieldset class="form-label-group position-relative has-icon-left">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required <?php
                                                                                                                                            if ($username) {
                                                                                                                                                echo 'autofocus';
                                                                                                                                            }
                                                                                                                                            ?>>
                                <div class="form-control-position">
                                    <i class="feather icon-lock"></i>
                                </div>
                                <!-- <label for="password">Password</label> -->
                            </fieldset>
                            <?php
                            if (validation_errors()) {
                                echo validation_errors();
                            }
                            ?>
                            <br>
                            <a href="" data-toggle='modal' data-target='#forgetModal'>Forgot password?</a><br>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#register" style="margin-bottom:20px">Sign up</button>
                            <button type="submit" class="btn btn-primary float-right btn-inline">Sign in</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                    <div class="login-footer">
                        <div class="divider">
                        </div>
                        <div class="footer-btn d-inline">
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <form id="reg">
        <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Registrasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <p class="col-sm-3 form-control-label">Username</p>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="usernamer" id="usernamer" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <p class="col-sm-3 form-control-label">Email</p>
                            <div class="col-sm-6">
                                <input type="email" class="form-control" name="email" pattern=".+@gmail.com" id="email" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <p class="col-sm-3 form-control-label">Password</p>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="passwordr" id="passwordr" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" onclick="registrasi()" class="btn btn-primary" id="btn-submit-reg">Sign Up</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form id="reset">
        <div class="modal fade" id="forgetModal" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-success">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Forgot Password</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <p class="col-sm-3 form-control-label">Username</p>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="username_fp" id="username_fp">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <p class="col-sm-3 form-control-label">Email</p>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="email_fp" id="email_fp">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" id="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btn-submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

</body>

</html>
<script src="node_modules\sweetalert2\dist/sweetalert2.all.min.js"></script>
<script src="node_modules\jquery\dist/jquery.js"></script>
<script src="node_modules\jquery\dist/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script type='text/javascript'>
    $(document).ready(function() {});

    function registrasi() {
        document.getElementById("btn-submit-reg").innerHTML = '<i class="fa fa-spinner fa-spin"></i> Processing...';
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('Login/registrasi'); ?>",
            dataType: "JSON",
            data: $('#reg').serialize(),
            success: function(result) {
                if (result.success) {

                    document.getElementById("btn-submit-reg").innerHTML = 'Simpan';
                    $('#modal-sd').modal('hide');
                    $('.modal-backdrop').remove();
                    swal.fire({
                        title: 'Data berhasil disimpan',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'Ok',
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: false,
                    }).then(function(result) {
                        window.location.reload();
                    });
                } else {
                    if (result.msg !== null) {
                        document.getElementById("btn-submit-reg").innerHTML = 'Simpan';
                        $('.modal-backdrop').remove();
                        Swal.fire({
                            type: 'warning',
                            title: 'Gagal!',
                            text: result.msg
                        });
                    }
                }
            },
            error: function(event, textStatus, errorThrown) {
                document.getElementById("btn-submit-reg").innerHTML = 'Simpan';
                $('#modal-sd').modal('hide');
                $('.modal-backdrop').remove();
                swal.fire("Error", "Gagal menyimpan data.", "error");
                console.log('Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown);
            }
        });
    }

    $("#reset").submit(function(event) {
        if ($('#username_fp').val() == "") {
            swal.fire("Maaf", "Harap Isi No HP.", "error")
        } else if ($('#email_fp').val() == "") {
            swal.fire("Maaf", "Harap Isi Email Alternatif.", "error")
        }else{
        event.preventDefault();
        document.getElementById("btn-submit").innerHTML = '<i class="fa fa-spinner fa-spin"></i> Processing...';
        $.ajax({
            url: "<?php echo site_url('LupaPassword/check_data') ?>",
            type: 'POST',
            dataType: 'json',
            data: $(this).serialize(),
            success: function(data) {
                if (data.success) {
                    swal.fire('Data Berhasil Dikirim!', 'Silahkan periksa email anda', 'success').then(function(result) {
                        window.location.reload();
                    });
                } else {
                    if (data.msg !== '') {
                        Swal.fire({
                            type: 'warning',
                            title: 'Gagal!',
                            text: data.msg
                        }).then(function(result) {
                            window.location.reload();
                        });
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Terjadi Kesalahan... Silahkan hubungi Administrator'
                        });
                    }
                }
            }

        });
        $.ajax({
            url: "<?php echo site_url('LupaPassword/email_reset_password_validation') ?>",
            type: 'POST',
            dataType: 'json',
            data: $(this).serialize(),
            success: function(data) {
                if (data.success) {} else {
                    if (data.msg !== '') {} else {}
                }
            }

        });
        }
    });
</script>