            <!-- END: Footer-->
            <script>
                // $.widget.bridge('uibutton', $.ui.button);
                var baseurl = "<?php echo base_url(); ?>";
            </script>


            <!-- BEGIN: Vendor JS-->
            <script src="<?= site_url(); ?>app-assets/vendors/js/vendors.min.js"></script>
            <!-- BEGIN Vendor JS-->

            <!-- BEGIN: Page Vendor JS-->
            <script src="<?= site_url(); ?>app-assets/vendors/js/ui/jquery.sticky.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/pickers/pickadate/picker.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/forms/select/select2.full.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/extensions/toastr.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/charts/apexcharts.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/extensions/tether.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/extensions/shepherd.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/ui/jquery.sticky.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
            <script src="<?= site_url(); ?>app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
            <script src="<?= site_url(); ?>assets/plugins/autocomplete/jquery.autocomplete.js"></script>
            <script src="<?= site_url(); ?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
            <script src="<?= site_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
            <script src="<?= site_url(); ?>assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
            <script src="<?= site_url(); ?>assets/plugins/moment/moment.js"></script>
            <script src="<?= site_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
            <!-- END: Page Vendor JS-->

            <!-- BEGIN: Theme JS-->
            <script src="<?= site_url(); ?>app-assets/js/core/app-menu.js"></script>
            <script src="<?= site_url(); ?>app-assets/js/core/app.js"></script>
            <script src="<?= site_url(); ?>app-assets/js/scripts/components.js"></script>
            <!-- END: Theme JS-->

            <!-- BEGIN: Page JS-->
            <!-- <script src="<?= site_url(); ?>app-assets/js/scripts/pages/dashboard-analytics.js"></script> -->
            <!-- END: Page JS-->

