<?php $this->load->view('header')?>

    <section class="col-9">
    <div class="row">
        <?php 
            $temp = array();
            foreach ($Mesin as $index => $row){
                $temp[$index] = $row->machine_name;
        ?>
        <div class="col-lg-4 listData">
            <label class="col-sm" style="color: #4a4a4a ;text-align: center; margin-left: 30px; font-size: 20px; font-weight: 800; font-family: Arial;"><?=$row->machine_name?></label>
            <input type="hidden" class="titleMesin" name="idnih" value="<?= $temp[$index] = $row->machine_name; ?>"> 
            <div class="col-md backgroundData" id="error">
                <button class="btn btn-primary" type="button" style='font-size:24px; transform: translateY(15%);'>
                    <a href="<?php echo site_url('table/table_view/' . $row->machine_name); ?>" style="color: white;">Table</a>
                </button>
                <button class="btn btn-primary" type="button" style='font-size:24px; transform: translateY(15%);'>
                    <a href="<?php echo site_url('chart/chart_view/' . $row->machine_name); ?>" style="color: white;">Chart</a>
                </button>
            </div>
        </div>
        <?php }?>
        <div class="col-lg">
            <div class="col-sm" style=" margin: 38px 0px 0 30px; text-align: center; height: 70px;">
                <button class="btn btn-primary" type="button" style='font-size:24px; transform: translateY(15%);'>
                    <a href="<?php echo site_url('manageTable'); ?>" style="color: white;">Manage Table</a>
                </button>
            </div>
        </div><br>
    </div>
    </section>
    </div>

<body>

</html>
    <script src="node_modules\sweetalert2\dist/sweetalert2.all.min.js"></script>
    <script src="node_modules\jquery\dist/jquery.js"></script>
    <script src="node_modules\jquery\dist/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>

    <script>
        $(document).ready(function() {
            var myVar = setInterval(getData, 3000);
            var get_id = $('#idnih').val();
            getData()

            // console.log(get_id);
        });

        function getData() {
            var get_id = $('#idnih').val();
            
            $(".listData").each(function(i) {
                var parent = $(this);
                var input = parent.find('input[name=idnih]').val()
                var background = parent.find('.backgroundData');
                
                
                $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('Beranda/get_error/') ?>" + input,
                success: function(data) {
            
                        if (data == "") {
                            // alert("Data Kosong");
                        } else {
                            background.removeClass('box-table')
                            background.removeClass('error-machine')
                            background.addClass(data.error)
                                // document.getElementById("error").className = data.error;
                        }
                },
                error: function() {
                    alert("error");
                }
                });
            });


            // console.log(get_id);
            // console.log();
          
        }
    </script>