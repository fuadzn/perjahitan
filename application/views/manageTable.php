<?php $this->load->view('header')?>
    <section class="col-9">
    <h1 > Management Table</h1>
    <?php echo $this->session->flashdata('success_msg'); ?>
    <table id="example" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Machine Name</th>
                <th>Action</th>
            </tr>
            <tbody>
            <?php 
                $i= 1;
                foreach ($Machine as $res) 
                {   
                    ?>
            <tr>
                <td><?php echo $i++ ?></td>
                <td><?php echo $res->machine_name ?></td>
                <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit" onclick="edit_machine(<?php echo $res->machine_id ?>)">Edit</button>   <button class="btn btn-danger" onclick="delete_table(<?php echo $res->machine_id ?>)">Delete</button></td>
            </tr>
            <?php
                }
                ?>
            </tbody>
        </tbody>
    </table>
    </section>
    </div>

    <?php echo form_open('ManageTable/update_machine');?>
    <form id="reg">
      <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <p class="col-sm-3 form-control-label">Old Name</p>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="machine_id" id="machine_id" hidden>
                                <input type="text" class="form-control" name="old_name" id="old_name" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-1"></div>
                            <p class="col-sm-3 form-control-label">New Name</p>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="new_name" id="new_name">
                            </div>
                        </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btn-submit-reg">Edit</button>
            </div>
            </div>
        </div>
        </div>
        </form>
        <?php echo form_close(); ?>
</body>

</html>
    <script src="node_modules\sweetalert2\dist/sweetalert2.all.min.js"></script>
    <script src="node_modules\jquery\dist/jquery.js"></script>
    <script src="node_modules\jquery\dist/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
    function edit_machine(id) {
        $.ajax({
            type:'POST',
            dataType: 'json',
            url:"<?php echo base_url('ManageTable/edit_machine')?>",
            data: {
                id: id
            },
            success: function(data){
                $('#old_name').val(data[0].machine_name);
                $('#machine_id').val(data[0].machine_id);
            },
            error: function(){
                alert("error");
            }
        });
    }

    function delete_table(id) {
        $.ajax({
            type:'POST',
            dataType: 'json',
            url:"<?php echo base_url('ManageTable/delete_table')?>",
            data: {
                id: id
            },
            success: function(data){
            console.log(id)
                if(data.status = 'success'){
                    Swal.fire({
                            title: 'Selesai',
                            text: 'Data Sudah Dihapus',
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok',
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: false,
                        }).then(function (result) {
                            window.location.reload();
                        });
                } else {
                    Swal.fire({
                            title: 'Error',
                            text: 'Error',
                            type: 'error',
                            showCancelButton: false,
                            confirmButtonText: 'Ok',
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: false,
                        }).then(function (result) {
                            window.location.reload();
                        });
                }
            },
            error: function(){
                alert("error");
            }
        });
    }
    
</script>