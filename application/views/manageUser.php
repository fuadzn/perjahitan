<?php $this->load->view('header') ?>
<section class="col-9">
    <h1> Management User</h1>
    <table id="example" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>UserName</th>
                <th>Created Date</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        <tbody>
            <?php
            $i = 1;
            foreach ($User as $res) {
            ?>
                <tr>
                    <td><?php echo $i++ ?></td>
                    <td><?php echo $res->username ?></td>
                    <td><?php echo $res->created_date ?></td>
                    <td><?php if ($res->status_user == 0) {
                            echo "Non Aktif";
                        } else {
                            echo "Aktif";
                        }

                        ?></td>
                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit" onclick="edit_user(<?php echo $res->userid ?>)">Edit</button>
                        <button class="btn btn-danger" onClick="delete_user(<?php echo $res->userid ?>)">Delete</button>
                        <?php if ($res->status_user == 0) { ?>
                            <button class="btn btn-success" onClick="aktifkan_user(<?php echo $res->userid ?>)">Aktifkan</button>
                        <?php } else { ?>
                            <button class="btn btn-danger" onClick="nonaktifkan_user(<?php echo $res->userid ?>)">Non Aktifkan</button>
                        <?php } ?>
                    </td>
                </tr>
            <?php
            }
            ?>
        </tbody>
        </thead>
    </table>
</section>
</div>

<?php echo form_open('ManageUser/update_user'); ?>
<form id="reg">
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <p class="col-sm-3 form-control-label">Username</p>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="userid" id="userid" hidden>
                            <input type="text" class="form-control" name="username" id="username">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <p class="col-sm-3 form-control-label">Password</p>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" name="passwordr" id="passwordr">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-1"></div>
                        <p class="col-sm-3 form-control-label">Role Type</p>
                        <div class="col-sm-6">
                            <select class="form-control" name="role_id" id="role_id">
                                <option selected>Select Role Type</option>
                                <option value="0">User</option>
                                <option value="1">Admin</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="btn-submit-reg">Edit</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php echo form_close(); ?>
</body>

</html>
<script src="node_modules\sweetalert2\dist/sweetalert2.all.min.js"></script>
<script src="node_modules\jquery\dist/jquery.js"></script>
<script src="node_modules\jquery\dist/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>
    function edit_user(id) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "<?php echo base_url('ManageUser/get_user_id') ?>",
            data: {
                id: id
            },
            success: function(data) {
                // $('#old_name').val(data[0].machine_name);
                $('#userid').val(data[0].userid);
            },
            error: function() {
                alert("error");
            }
        });
    }

    function delete_user(id) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "<?php echo base_url('ManageUser/delete_user') ?>",
            data: {
                id: id
            },
            success: function(data) {
                console.log(id)
                if (data.status = 'success') {
                    Swal.fire({
                        title: 'Selesai',
                        text: 'Data Sudah Dihapus',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'Ok',
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: false,
                    }).then(function(result) {
                        window.location.reload();
                    });
                } else {
                    Swal.fire({
                        title: 'Error',
                        text: 'Error',
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonText: 'Ok',
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: false,
                    }).then(function(result) {
                        window.location.reload();
                    });
                }
            },
            error: function() {
                alert("error");
            }
        });
    }

    function aktifkan_user(id) {
        console.log(id);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "<?php echo base_url('ManageUser/aktif_user') ?>",
            data: {
                id: id
            },
            success: function(data) {
                console.log(id)
                if (data.status = 'success') {
                    Swal.fire({
                        title: 'Selesai',
                        text: 'Data Berhasil Diaktifkan',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'Ok',
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: false,
                    }).then(function(result) {
                        window.location.reload();
                    });
                } else {
                    Swal.fire({
                        title: 'Error',
                        text: 'Error',
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonText: 'Ok',
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: false,
                    }).then(function(result) {
                        window.location.reload();
                    });
                }
            },
            error: function() {
                alert("error");
            }
        });
    }

    function nonaktifkan_user(id) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "<?php echo base_url('ManageUser/nonaktif_user') ?>",
            data: {
                id: id
            },
            success: function(data) {
                console.log(id)
                if (data.status = 'success') {
                    Swal.fire({
                        title: 'Selesai',
                        text: 'Data Berhasil Dinonaktifkan',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'Ok',
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: false,
                    }).then(function(result) {
                        window.location.reload();
                    });
                } else {
                    Swal.fire({
                        title: 'Error',
                        text: 'Error',
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonText: 'Ok',
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: false,
                    }).then(function(result) {
                        window.location.reload();
                    });
                }
            },
            error: function() {
                alert("error");
            }
        });
    }
</script>