<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_Login extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function logged_id(){
		return $this->session->userdata('id_user');
	}

	public function check_login($username,$password){
		$this->db->where('username', $username);
		$query = $this->db->get('user');
		if ($query->num_rows() == 1) {
		 $hash = $query->row('password');
		 if (password_verify($password,$hash)){
		  return $query->result();
		 } else {
		  echo "Wrong Password. Try again.";
		 }
		} else {
		 echo "Account is not existed.";
		}
	}

	private function hash_password($password) {
		return password_hash($password, PASSWORD_BCRYPT);
	}

	function is_logged_in()
	{
		return $this->session->userdata('userid') != false;
	}

	function get_user($username)
	{
		return $this->db->query("SELECT * from user where deleted = 0 and username COLLATE latin1_general_cs LIKE '$username'");
	}

	public function get_all_user()
	{
		$this->db->select('*');
		$this->db->from('user');
		return $this->db->get();
	}

	function registrasi($data)
	{
		return $this->db->insert('user', $data);
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

	function has_permission($url, $userid)
	{
		//if no module_id is null, allow access
		if ($url == null or $url == 'beranda' or $url == 'logout') {
			return true;
		} else {
		return false;
		}
	}

	function get_logged_in_user_info()
	{
		$userid = $this->session->userdata('userid');
		if (($userid)) {
			return $this->get_info($userid);
		}
	}

	function get_info($userid)
	{
		$this->db->select('userid, username, role_id');
		$this->db->from('user');
		$this->db->where('userid', $userid);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			//Get empty base parent object, as $item_id is NOT an item
			$data_obj = new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('user');

			foreach ($fields as $field) {
				$data_obj->$field = '';
			}

			return $data_obj;
		}
	}

	function check_username($username) {
		return $this->db->query("SELECT * FROM user WHERE username = '$username'");
	}

	function check_data($username, $email) {
		return $this->db->query("SELECT * FROM user WHERE username = '$username' AND email='$email'");
	}

	public function check_reset_key($reset_key)
	{
		$this->db->where('reset_password', $reset_key);
		$this->db->from('user');
		return $this->db->count_all_results();
	}

	function get_username($reset_key)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('reset_password', $reset_key);
		return $this->db->get();
	}

	public function reset_password($reset_key, $password)
	{
		$this->db->where('reset_password', $reset_key);
		$data = array('password' => $password);
		$this->db->update('user', $data);
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	function update_reset_key($email, $reset_key)
	{
		$this->db->where('email', $email);
		$data = array('reset_password' => $reset_key);
		$this->db->update('user', $data);
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function get_id_user($id)
	{
		$sql = "SELECT * FROM user WHERE userid = ?";
		return $this->db->query($sql, array($id));
	}

	function update_user($table, $data, $where)
	{
		return $this->db->update($table, $data, $where);
	}

	function deleteUser($id)
	{
		return $this->db->delete('user', array('userid' => $id));
	}
}