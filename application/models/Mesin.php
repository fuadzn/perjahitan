<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mesin extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function get_all_mesin()
	{
		$this->db->select('*');
		$this->db->from('list_machine');
		return $this->db->get();
	}

	public function get_mesin($tgl, $tgl_akhir, $table)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('LEFT(time,10)>=', $tgl);
		$this->db->where('LEFT(time,10)<=', $tgl_akhir);
		return $this->db->get();
	}

	function get_data($table, $tgl, $tgl_akhir)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('LEFT(time,10)>=', $tgl);
		$this->db->where('LEFT(time,10)<=', $tgl_akhir);
		return $this->db->get();
	}

	function get_error_machine($table)
	{
		return $this->db->query("SELECT * FROM $table ORDER BY id DESC LIMIT 1");
	}

	public function get_all_machine()
	{
		$this->db->select('*');
		$this->db->from('list_machine');
		return $this->db->get();
	}

	public function get_id_machine($id)
	{
		$sql = "SELECT * FROM list_machine WHERE machine_id = ?";
		return $this->db->query($sql, array($id));
	}

	function update_table($table, $data, $where)
	{
		return $this->db->update($table, $data, $where);
	}

	function deleteTable($id)
	{
		return $this->db->delete('list_machine', array('machine_id' => $id));
	}

	function rename_table($newTableName, $tableName)
	{
		return $this->db->query("RENAME TABLE `u715389008_Alat01`.`$tableName` TO `u715389008_Alat01`.`$newTableName`;");
	}
}